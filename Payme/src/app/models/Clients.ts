export class Clients {

    nombre_completo: string; //
    correo_electronico: string; //
    cedula: string; //
    celular: string; //
    telefono: string; //
    direccion: string; //

    estado_civil: string; //notinput //
    estado_laboral: string; //notinput //

    sueldo: string; //
    modalidad_sueldo:string; //notinput //
    ingresos_adicionales: string; //

    garante_nombre: string;
    garante_celular: string;
    garante_email: string;

    pariente_nombre: string;
    pariente_celular: string;
    pariente_email: string;
    pariente_parentezco: string;

}