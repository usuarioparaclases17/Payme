export class Prestamo {

    id_cliente: string;
    id_prestamo: string;
    id_usuario: string;

    nombre_cliente: string;
    monto: string;
    activo: boolean;

    amortizacion: string;
    periodo_pago: string;       //(LISTA: DIARIO, SEMANAL, QUINCENAL, MENSUAL, ANUAL)
    porciento_interes: string;
    cuotas: string;
    fecha_creacion: string;

    pagoMensual: string;
    interesTotal: string;
    pagoTotal: string;

}