import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ReactiveFormsModule} from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


//FireBase Imports
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { InicioComponent } from './components/inicio/inicio.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { ClientesListaComponent } from './components/clientes/clientes-lista/clientes-lista.component';
import { ClientesInfoComponent } from './components/clientes/clientes-lista/clientes-info/clientes-info.component';
import { ClientesPrestamosComponent } from './components/clientes/clientes-lista/clientes-prestamos/clientes-prestamos.component';
import { CrearClientesComponent } from './components/clientes/crear-clientes/crear-clientes.component';
import { HomeComponent } from './components/inicio/home/home.component';
import { TableroComponent } from './components/inicio/tablero/tablero.component';
import { PagosRealizadosComponent } from './components/pagos-realizados/pagos-realizados.component';
import { PagosComponent } from './components/pagos-realizados/pagos/pagos.component';
import { InformacionPagosComponent } from './components/pagos-realizados/pagos/informacion-pagos/informacion-pagos.component';
import { PrestamosComponent } from './components/prestamos/prestamos.component';
import { PrestamosListaClientesComponent } from './components/prestamos/prestamos-lista-clientes/prestamos-lista-clientes.component';
import { CrearPrestamosComponent } from './components/prestamos/prestamos-lista-clientes/crear-prestamos/crear-prestamos.component';
import { LoadingSpinnerBlackComponent } from './ui/loading-spinner-black/loading-spinner-black.component';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';
  //Database config
  const firebaseConfig = {
    apiKey: "AIzaSyBnckz_VJ9HJC5eVU3b7O6ttUlfaw6X_F0",
    authDomain: "payme-67dd4.firebaseapp.com",
    databaseURL: "https://payme-67dd4.firebaseio.com",
    projectId: "payme-67dd4",
    storageBucket: "payme-67dd4.appspot.com",
    messagingSenderId: "924364958976",
    appId: "1:924364958976:web:ffa61f466c069df44257e5"
  };


@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    ClientesListaComponent,
    ClientesInfoComponent,
    ClientesPrestamosComponent,
    CrearClientesComponent,
    InicioComponent,
    HomeComponent,
    TableroComponent,
    PagosRealizadosComponent,
    PagosComponent,
    InformacionPagosComponent,
    PrestamosComponent,
    PrestamosListaClientesComponent,
    CrearPrestamosComponent,
    LoadingSpinnerBlackComponent,
    LoadingSpinnerComponent
  ],
  entryComponents: [],
  imports: [
    ReactiveFormsModule, //Forms
    BrowserModule, 
    IonicModule.forRoot(), 
    AppRoutingModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule,
    NgbModule // storage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
