import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { AngularFireAuth } from "@angular/fire/auth";
import { format } from "date-fns";
import { Clients } from "../models/Clients";
import { NavController } from "@ionic/angular";
import { Prestamo } from "../models/Prestamo";

@Injectable({
  providedIn: "root"
})
export class DbService {
  cliente_id: string;
  cliente_conPrestamo: any;
  cliente_sinPrestamo: any;

  prestamo_data: any;
  cliente_data: any;
  all_data: any;

  prestamo: Prestamo = new Prestamo();

  constructor(
    private navCtrl: NavController,
    private afAuth: AngularFireAuth,
    private database: AngularFirestore
  ) {
    //Cargar clientes
  }

  getUsers() {
    const uid = this.afAuth.auth.currentUser.uid;
    return this.database
      .collection("users")
      .doc(uid)
      .valueChanges();
  }

  //Añadir al cliente.
  async add_cliente(newClient: Clients) {
    const fecha_creacion_de_usuario = format(
      new Date(),
      "yyyy-MM-dd hh:mm:ss a"
    );
    const uid = this.afAuth.auth.currentUser.uid;
    const client_id = this.database.createId();

    let ResgistramosElCliente = new Promise((resolve, reject) => {
      this.database
        .collection("clients")
        .doc(client_id)
        .set({
          id_usuario: uid,
          id_cliente: client_id,
          nombre_completo: newClient.nombre_completo,
          cedula: newClient.cedula,
          correo_electronico: newClient.correo_electronico,
          telefono: newClient.telefono,
          celular: newClient.celular,
          direccion: newClient.direccion,
          estado_civil: newClient.estado_civil,
          estado_laboral: newClient.estado_laboral,
          sueldo: newClient.sueldo,
          modalidad_sueldo: newClient.modalidad_sueldo,

          ingresos_adicionales: newClient.ingresos_adicionales,

          garante_nombre: newClient.garante_nombre,
          garante_celular: newClient.garante_celular,
          garante_email: newClient.garante_email,

          pariente_nombre: newClient.pariente_nombre,
          pariente_celular: newClient.pariente_celular,
          pariente_email: newClient.pariente_email,
          pariente_parentezco: newClient.pariente_parentezco,

          fecha_creacion: fecha_creacion_de_usuario
        });
    }).then(() => {
      this.navCtrl.navigateForward("/menu/inicio/tabs/clientes");
    });
  }

  //Obtener los clientes de Ese usuario
  async getClients_conPrestamos() {
    let ids_prestamo = [];
    let clientes_conPrestamo = [];

    let buscamosLosPrestamosActivos = new Promise((resolve, reject) => {
      this.database
        .collection("prestamos", prestamo =>
          prestamo.where("activo", "==", true)
        )
        .valueChanges()
        .subscribe(prestamo => {
          prestamo.forEach(prestamo => {
            ids_prestamo.push(prestamo["id_cliente"]);
          });
          resolve(ids_prestamo);
        });
    });

    buscamosLosPrestamosActivos.then(data => {
      ids_prestamo.forEach(cliente => {
        this.database
          .collection("clients", clientes =>
            clientes.where("id_cliente", "==", cliente)
          )
          .valueChanges()
          .subscribe(cliente_conprestamo => {
            cliente_conprestamo.forEach(cliente => {
              clientes_conPrestamo.push(cliente);
            });
          });
      });
    });
    return clientes_conPrestamo;
  }

  //El get prestamo con promesas
  async getClients_sinPrestamos() {
    let todosLosClientes: Array<string> = [];
    let clientesConPrestamosActivos: Array<string> = [];

    var ids_clientes_disponibles = [];
    let clientes_disponibles = [];

    //Vemos cuales clientes tienen prestamos Inactivos
    let buscamos_losclientes = new Promise((resolve, reject) => {
      this.database
        .collection("clients", clientes =>
          clientes.where("id_usuario", "==", this.afAuth.auth.currentUser.uid)
        )
        .valueChanges()
        .subscribe(clientes => {
          clientes.forEach(cliente => {
            todosLosClientes.push(cliente["id_cliente"]);
          });
        });

      this.database
        .collection("prestamos", prestamos =>
          prestamos
            .where("id_usuario", "==", this.afAuth.auth.currentUser.uid)
            .where("activo", "==", true)
        )
        .valueChanges()
        .subscribe(prestamos => {
          prestamos.forEach(prestamo => {
            clientesConPrestamosActivos.push(prestamo["id_cliente"]);
          });
          resolve(clientesConPrestamosActivos);
        });
    });

    buscamos_losclientes
      .then(() => {
        todosLosClientes.forEach(cliente => {
          if (!clientesConPrestamosActivos.includes(cliente)) {
            ids_clientes_disponibles.push(cliente);
          } else {
          }
        });
      })
      .then(() => {
        ids_clientes_disponibles.forEach(cliente_disponible => {
          this.database
            .collection("clients", cliente =>
              cliente.where("id_cliente", "==", cliente_disponible)
            )
            .valueChanges()
            .subscribe(clientes => {
              clientes.forEach(cliente => {
                clientes_disponibles.push(cliente);
              });
            });
        });
      });
    return clientes_disponibles;
  }

  async getPrestamos(id_cliente: string) {
    let prestamos = [];
    this.database
      .collection("prestamos", prestamo =>
        prestamo
          .where("id_cliente", "==", id_cliente)
          .where("activo", "==", true)
      )
      .valueChanges()
      .subscribe(data => {
        prestamos = data;
      });
    return prestamos;
  }

  //Prestamos
  agregar_Prestamo(prestamo: any, id_cliente, nombre_del_cliente) {
    const uid = this.afAuth.auth.currentUser.uid;
    const fecha_creacion_de_usuario = format(
      new Date(),
      "yyyy-MM-dd hh:mm:ss a"
    );
    const prestamo_id = this.database.createId();

    this.database
      .collection("prestamos")
      .doc(prestamo_id)
      .set({
        id_cliente: id_cliente,
        id_usuario: uid,
        id_prestamo: prestamo_id,

        fecha_creacion: fecha_creacion_de_usuario,

        monto: prestamo.monto,
        nombre_cliente: nombre_del_cliente,
        activo: true,
        amortizacion: prestamo.amortizacion,
        periodo_pago: prestamo.periodo_pago,
        cuotas: prestamo.cuotas,
        porciento_interes: prestamo.periodo_pago,
        pagoMensual: prestamo.pagoMensual,
        interesTotal: prestamo.interesTotal,
        pagoTotal: prestamo.pagoTotal
      });
  }

  //Cancelar prestamo.
  async cancelar_Prestamo(id_prestamo) {
    this.database
      .collection("prestamos")
      .doc(id_prestamo)
      .update({
        activo: false
      });
  }

  //Enviar el nombre del Cliente para crear prestamo.
  async EnviarA_CrearPrestamoConDatos() {
    return this.cliente_data;
  }

  //Obtener los Datos del prestamo del cliente.
  async ObtenerDatosDelPrestamo(id_cliente: string, cliente_info: any) {
    //Asignamos el tiempo a los clientes.
    this.cliente_data = cliente_info;

    //Mira el préstamo activo del cliente.
    this.database
      .collection("prestamos", prestamo =>
        prestamo
          .where("id_cliente", "==", id_cliente)
          .where("activo", "==", true)
      ) //
      .valueChanges()
      .subscribe(async data => {
        this.prestamo_data = await data[0];
      });
  }

  enviarDatosDelPrestamo() {
    this.all_data = [this.cliente_data, this.prestamo_data];
    return this.all_data;
  }

  //Enviar datos de información del cliente
  obtenerInformacionCliente(cliente_info: any) {
    this.cliente_data = cliente_info;
  }
  enviarInformacionCliente() {
    return this.cliente_data;
  }

  //Actualizar informacion del cliente.
  updateCliente(newClient: Clients, id_cliente) {
    this.database
      .collection("clients")
      .doc(id_cliente)
      .update({
        nombre_completo: newClient.nombre_completo,
        cedula: newClient.cedula,
        correo_electronico: newClient.correo_electronico,
        telefono: newClient.telefono,
        celular: newClient.celular,
        direccion: newClient.direccion,
        estado_civil: newClient.estado_civil,
        estado_laboral: newClient.estado_laboral,
        sueldo: newClient.sueldo,
        modalidad_sueldo: newClient.modalidad_sueldo,

        ingresos_adicionales: newClient.ingresos_adicionales,

        garante_nombre: newClient.garante_nombre,
        garante_celular: newClient.garante_celular,
        garante_email: newClient.garante_email,

        pariente_nombre: newClient.pariente_nombre,
        pariente_celular: newClient.pariente_celular,
        pariente_email: newClient.pariente_email,
        pariente_parentezco: newClient.pariente_parentezco
      });
  }
}
