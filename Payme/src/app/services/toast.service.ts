import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';
@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastCtrl: ToastController) {}

  async mostrarNotificacion(mensaje: string) {
    const toast = await this.toastCtrl.create({
      message: mensaje,
      color: 'primary',
      duration: 2000
    });
    toast.present();
  }




}
