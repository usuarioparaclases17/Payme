import { Injectable } from "@angular/core";
import { DbService } from "../services/db.service";
import { Resolve } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class Resolve3Service implements Resolve<any> {
  constructor(private dbSvc: DbService) {}

  async resolve() {
    return this.dbSvc.enviarInformacionCliente();
  }
}

//
