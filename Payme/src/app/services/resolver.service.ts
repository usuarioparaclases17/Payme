import { Injectable } from "@angular/core";
import { DbService } from "../services/db.service";
import { Resolve, ActivatedRouteSnapshot } from "@angular/router";

@Injectable({
  providedIn: "root"
})
export class ResolverService implements Resolve<any> {
  constructor(private dbSvc: DbService) {}

  async resolve(route: ActivatedRouteSnapshot) {
    return this.dbSvc.enviarDatosDelPrestamo();
  }
}
