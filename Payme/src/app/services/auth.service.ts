import { Injectable } from "@angular/core";
import { AngularFireAuth } from "@angular/fire/auth";
import { AngularFirestore } from "@angular/fire/firestore";
import { User } from "../models/Users";
import { Router } from "@angular/router";
import { ToastService } from "./toast.service";
import { format } from "date-fns";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  public isLogged: any = false;

  constructor(
    private toastService: ToastService,
    private afAuth: AngularFireAuth,
    private router: Router,
    private database: AngularFirestore
  ) {
    afAuth.authState.subscribe(user => (this.isLogged = user));
  }

  //Metodo para Logearse :o
  async Logearse(user: User) {
    try {
      return await this.afAuth.auth.signInWithEmailAndPassword(
        user.email,
        user.contra
      );
    } catch (err) {
      const err_1 =
        "There is no user record corresponding to this identifier. The user may have been deleted.";
      const err_2 =
        "The password is invalid or the user does not have a password.";
      const err_3 = "The email address is badly formatted.";

      switch (err.message) {
        case err_1: {
          this.toastService.mostrarNotificacion(
            "Usuario o contraseña incorrectos / as."
          );
          break;
        }
        case err_2: {
          this.toastService.mostrarNotificacion("Contraseña incorrecta.");
          break;
        }
        case err_3: {
          this.toastService.mostrarNotificacion(
            "Ingrese un correo electrónico o usuario válido."
          );
          break;
        }
        default: {
          this.toastService.mostrarNotificacion(
            "Revise sus credenciales e inténtelo de nuevo."
          );
          console.log("❗❗ Error:\n", err.message);

          break;
        }
      }
    }
  } //End registrar Usuario

  //DesLogearse
  desLogearse() {
    this.afAuth.auth.signOut();
    this.router.navigateByUrl("/login");
  }

  //Registro
  async Registrarse_authSvc(user: User) {
    //Formateo de fechas
    var fecha_nacimiento = format(
      new Date(user.fecha_nacimiento),
      "yyyy-MM-dd"
    );
    var fecha_creacion_de_usuario = format(new Date(), "yyyy-MM-dd hh:mm:ss a");
    try {
      //Creacion de usuarios e INSERT en la base de datos
      const res = await this.afAuth.auth.createUserWithEmailAndPassword(
        user.email,
        user.contra
      );

      //id única de usuario
      const uid = res.user.uid;
      this.database
        .collection("users")
        .doc(uid)
        .set({
          nombre_usuario: user.nombre_usuario,
          contraseña: user.contra,
          correo_electrónico: user.email,
          nombre_completo: user.nombre_completo,
          telefono: user.telefono,
          celular: user.celular,
          fecha_nacimiento: fecha_nacimiento,
          fecha_creacion_de_usuario: fecha_creacion_de_usuario
        });
      return res;
    } catch (err) {
      //error handler

      const err_1 =
        'createUserWithEmailAndPassword failed: First argument "email" must be a valid string.';
      const err_2 = "The email address is already in use by another account.";
      const err_3 = "The password must be 6 characters long or more.";
      const err_4 =
        "Function DocumentReference.set() called with invalid data. Unsupported field value: undefined (found in field telefono)";
      const err_6 = "The email address is badly formatted.";
      const err_7 =
        "Function DocumentReference.set() called with invalid data. Unsupported field value: undefined (found in field nombre)";
      const err_8 =
        'createUserWithEmailAndPassword failed: Second argument "password" must be a valid string.';
      const err_9 = "Password should be at least 6 characters";

      switch (err.message) {
        case err_1: {
          this.toastService.mostrarNotificacion(
            "Porfavor introduzca un correo electrónico."
          );
          break;
        }
        case err_2: {
          this.toastService.mostrarNotificacion(
            "Este correo electrónico ya está siendo utilizado por alguien más."
          );
          break;
        }
        case err_3: {
          this.toastService.mostrarNotificacion(
            "Su contraseña debe tener un mínimo de 6 carácteres. "
          );
          break;
        }
        case err_4: {
          this.toastService.mostrarNotificacion(
            "Porfavor introduzca un número de teléfono."
          );
          break;
        }
        case err_6: {
          this.toastService.mostrarNotificacion(
            "Introduzca un correo electrónico válido. "
          );
          break;
        }
        case err_7: {
          this.toastService.mostrarNotificacion(
            "Usuario o contraseña incorrectos / as."
          );
          break;
        }
        case err_8: {
          this.toastService.mostrarNotificacion(
            "Porfavor introduzca una contraseña."
          );
          break;
        }
        case err_9: {
          this.toastService.mostrarNotificacion(
            "Su contraseña debe tener un mínimo de 6 carácteres. "
          );
          break;
        }
        default: {
          this.toastService.mostrarNotificacion(
            "Revise sus credenciales antes de registrarse"
          );
          console.log("❗❗Copiar:\n", err.message);
          break;
        }
      }
    }
  }
} //T H E   E N  D

//(OLD) Registro
// async Registrarse(user: User){
//   this.database.collection('Users').add(this.user);
//   try {
//  return await this.afAuth.auth.createUserWithEmailAndPassword(user.email, user.password).then(() => this.router.navigateByUrl('/'));

//   } catch(err){
//     console.log(err);
//     }
//  //End registrar Usuario
