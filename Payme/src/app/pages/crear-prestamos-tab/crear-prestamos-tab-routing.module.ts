import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearPrestamosTabPage } from './crear-prestamos-tab.page';

const routes: Routes = [
  {
    path: '',
    component: CrearPrestamosTabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearPrestamosTabPageRoutingModule {}
