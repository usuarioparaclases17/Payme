import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearPrestamosTabPage } from './crear-prestamos-tab.page';

describe('CrearPrestamosTabPage', () => {
  let component: CrearPrestamosTabPage;
  let fixture: ComponentFixture<CrearPrestamosTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearPrestamosTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearPrestamosTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
