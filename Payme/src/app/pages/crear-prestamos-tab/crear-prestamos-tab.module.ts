import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearPrestamosTabPageRoutingModule } from './crear-prestamos-tab-routing.module';

import { CrearPrestamosTabPage } from './crear-prestamos-tab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearPrestamosTabPageRoutingModule
  ],
  declarations: [CrearPrestamosTabPage]
})
export class CrearPrestamosTabPageModule {}
