import { Component, OnInit } from "@angular/core";
import { DbService } from "../../services/db.service";
import { Prestamo } from "../../models/Prestamo";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-crear-prestamos-tab",
  templateUrl: "./crear-prestamos-tab.page.html",
  styleUrls: ["./crear-prestamos-tab.page.scss"]
})
export class CrearPrestamosTabPage implements OnInit {
  selectedVal = { Number: 1 };

  prestamo: Prestamo = new Prestamo();

  cliente_data: any;
  cliente_id: any;
  cliente_nombre: any;
  campos_incompletos: any;

  constructor(
    private router: Router,
    private DbSvc: DbService,
    private route: ActivatedRoute
  ) {}

  async ngOnInit() {
    await this.waitforData();
  }

  async waitforData() {
    if (this.route.snapshot.data["special"]) {
      this.cliente_data = this.route.snapshot.data["special"];
      this.cliente_nombre = this.cliente_data.nombre_completo;
      this.cliente_id = this.cliente_data.id_cliente;
    }
  }

  agregar_Prestamos() {
    const CapitalInicial = this.prestamo.monto;
    const Tinteres = this.prestamo.porciento_interes;
    const Years = this.prestamo.cuotas;

    //calculando prestamo

    const capital = parseFloat(CapitalInicial);
    const calcularinteres = parseFloat(Tinteres) / 100 / 12;
    const calcularpagos = parseFloat(Years) * 12;

    const x = Math.pow(1 + calcularinteres, calcularpagos);
    const mensual = (capital * x * calcularinteres) / (x - 1);
    const pagoMensual1 = mensual.toFixed(2);

    const InteresTotal1 = (mensual * calcularpagos - capital).toFixed(2);

    //Compute Total Payment

    const PagoTotal1 = (mensual * calcularpagos).toFixed(2);

    this.prestamo.pagoMensual = pagoMensual1;
    this.prestamo.interesTotal = InteresTotal1;
    this.prestamo.pagoTotal = PagoTotal1;

    this.DbSvc.agregar_Prestamo(
      this.prestamo,
      this.cliente_id,
      this.cliente_nombre
    );
    this.router.navigateByUrl("/menu/inicio/tabs/clientes");

    this.prestamo.monto = null;
    this.prestamo.periodo_pago = "";
    this.prestamo.porciento_interes = null;
    this.prestamo.cuotas = null;
  }
}
