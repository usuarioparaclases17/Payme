import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClientesTabPage } from './clientes-tab.page';

describe('ClientesTabPage', () => {
  let component: ClientesTabPage;
  let fixture: ComponentFixture<ClientesTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClientesTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
