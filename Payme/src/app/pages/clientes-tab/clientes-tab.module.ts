import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClientesTabPageRoutingModule } from './clientes-tab-routing.module';
import {SharedComponentesModule} from '../shared-componentes.module';
import { ClientesTabPage } from './clientes-tab.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClientesTabPageRoutingModule,
    SharedComponentesModule
  ], 
  entryComponents: [],
  declarations: [ ClientesTabPage]
})
export class ClientesTabPageModule {}
