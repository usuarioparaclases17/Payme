import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { InicioPage } from './inicio.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: InicioPage,
    children: [
      {
        path: 'inicio-tab',
        loadChildren: () => import('../inicio-tab/inicio-tab.module').then(m => m.InicioTabPageModule)
      },
      {
        path: 'clientes-tab',
        loadChildren: () => import('../clientes-tab/clientes-tab.module').then(m => m.ClientesTabPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/inicio-tab',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class InicioPageRoutingModule {}
