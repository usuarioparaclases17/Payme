import { Component, OnInit } from "@angular/core";
import { Router, RouterEvent } from "@angular/router";
import { AuthService } from "../../services/auth.service";

@Component({
  selector: "app-menu",
  templateUrl: "./menu.page.html",
  styleUrls: ["./menu.page.scss"]
})
export class MenuPage implements OnInit {
  pages = [
    {
      title: "Inicio",
      url: "/menu/inicio"
    },
    {
      title: "Crear Prestamos",
      url: "/menu/prestamos"
    },
    {
      title: "Clientes",
      url: "/menu/clientes"
    }
  ];

  selectedPath = "";

  constructor(private router: Router, private authSvc: AuthService) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }
  ngOnInit() {}
  desLogearse() {
    this.authSvc.desLogearse();
  }
}
