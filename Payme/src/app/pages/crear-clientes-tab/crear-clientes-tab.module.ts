import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CrearClientesTabPageRoutingModule } from './crear-clientes-tab-routing.module';
import { ReactiveFormsModule } from '@angular/forms';

import { CrearClientesTabPage } from './crear-clientes-tab.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CrearClientesTabPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CrearClientesTabPage]
})
export class CrearClientesTabPageModule {}
