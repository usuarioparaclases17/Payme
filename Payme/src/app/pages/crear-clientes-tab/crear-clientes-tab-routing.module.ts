import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CrearClientesTabPage } from './crear-clientes-tab.page';

const routes: Routes = [
  {
    path: '',
    component: CrearClientesTabPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CrearClientesTabPageRoutingModule {}
