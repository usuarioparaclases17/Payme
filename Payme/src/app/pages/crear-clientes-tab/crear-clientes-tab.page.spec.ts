import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CrearClientesTabPage } from './crear-clientes-tab.page';

describe('CrearClientesTabPage', () => {
  let component: CrearClientesTabPage;
  let fixture: ComponentFixture<CrearClientesTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CrearClientesTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CrearClientesTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
