import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InicioTabPageRoutingModule } from './inicio-tab-routing.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { InicioTabPage } from './inicio-tab.page';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    IonicModule,
    InicioTabPageRoutingModule
  ],
  declarations: [InicioTabPage]
})
export class InicioTabPageModule {}
