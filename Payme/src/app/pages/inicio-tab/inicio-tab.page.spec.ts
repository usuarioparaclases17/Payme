import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InicioTabPage } from './inicio-tab.page';

describe('InicioTabPage', () => {
  let component: InicioTabPage;
  let fixture: ComponentFixture<InicioTabPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InicioTabPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InicioTabPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
