import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientesPage } from './clientes.page';
import { ClientesListaComponent } from 'src/app/components/clientes/clientes-lista/clientes-lista.component';

const routes: Routes = [
  {
    path: 'tabs',
    component: ClientesPage,
    children: [
      {
        path: 'clientes-tab',
        component: ClientesListaComponent,
        pathMatch: 'full'
      },
      // {
      //   path: 'clientes-tab',
      //   loadChildren: () => import('../clientes-tab/clientes-tab.module').then(m => m.ClientesTabPageModule)
      // },
      {
        path: 'crear-clientes-tab',
        loadChildren: () => import('../crear-clientes-tab/crear-clientes-tab.module').then(m => m.CrearClientesTabPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/clientes-tab',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientesPageRoutingModule {}
