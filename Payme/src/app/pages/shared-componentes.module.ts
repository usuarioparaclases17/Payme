import { NgModule } from "@angular/core";
import { LoadingSpinnerComponent } from "../ui/loading-spinner/loading-spinner.component";
import { LoadingSpinnerBlackComponent } from "../ui/loading-spinner-black/loading-spinner-black.component";

@NgModule({
  declarations: [LoadingSpinnerComponent, LoadingSpinnerBlackComponent],
  exports: [LoadingSpinnerComponent, LoadingSpinnerBlackComponent]
})
export class SharedComponentesModule {}
