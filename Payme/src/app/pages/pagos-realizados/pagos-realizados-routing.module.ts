import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PagosRealizadosPage } from './pagos-realizados.page';

const routes: Routes = [
  {
    path: '',
    component: PagosRealizadosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PagosRealizadosPageRoutingModule {}
