import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PagosRealizadosPageRoutingModule } from './pagos-realizados-routing.module';

import { PagosRealizadosPage } from './pagos-realizados.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PagosRealizadosPageRoutingModule
  ],
  declarations: [PagosRealizadosPage]
})
export class PagosRealizadosPageModule {}
