import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PagosRealizadosPage } from './pagos-realizados.page';

describe('PagosRealizadosPage', () => {
  let component: PagosRealizadosPage;
  let fixture: ComponentFixture<PagosRealizadosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PagosRealizadosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PagosRealizadosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
