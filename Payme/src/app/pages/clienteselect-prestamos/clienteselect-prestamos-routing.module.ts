import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClienteselectPrestamosPage } from './clienteselect-prestamos.page';

const routes: Routes = [
  {
    path: '',
    component: ClienteselectPrestamosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClienteselectPrestamosPageRoutingModule {}
