import { Component, OnInit } from "@angular/core";
import { DbService } from "../../services/db.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: "app-clienteselect-prestamos",
  templateUrl: "./clienteselect-prestamos.page.html",
  styleUrls: ["./clienteselect-prestamos.page.scss"]
})
export class ClienteselectPrestamosPage implements OnInit {
  CargandoClientesDisponibles_spnner: boolean = true;

  clientes_disponibles: any = [];
  hay_clientes: boolean = true;

  nombre_cliente = "Armario Francisco";
  telefono_cliente = "809-724-6038";

  constructor(
    private dbSvc: DbService,
    private route: ActivatedRoute,
    private router: Router
  ) {
    this.ObtenerClientes_disponibles();
  }

  ngOnInit() {}

  async ObtenerClientes_disponibles() {
    this.clientes_disponibles = await this.dbSvc.getClients_sinPrestamos();
    this.CargandoClientesDisponibles_spnner = false;
  }
  soyunaflecha() {
    console.log("FLECHASTE");
  }
  soyunaflecha1() {
    console.log("ICONO");
  }

  //Para abrir el crear prestamo

  // Ver los datos de ese cliente
  abrirInformacionCliente(cliente: any) {
    this.dbSvc.obtenerInformacionCliente(cliente);
    this.router.navigateByUrl(`/clientes-info/${cliente.nombre_completo}`);
  }

  async irACrearPrestamos(cliente_data: any) {
    this.dbSvc.ObtenerDatosDelPrestamo(cliente_data.id_cliente, cliente_data);
    this.router.navigateByUrl(
      `/crear-prestamos-tab/${cliente_data.nombre_completo}`
    );
  }
}
