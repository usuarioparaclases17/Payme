import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClienteselectPrestamosPageRoutingModule } from './clienteselect-prestamos-routing.module';
import { SharedComponentesModule } from '../shared-componentes.module';

import { ClienteselectPrestamosPage } from './clienteselect-prestamos.page';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClienteselectPrestamosPageRoutingModule,
    SharedComponentesModule,
  ],
  declarations: [ClienteselectPrestamosPage]
})
export class ClienteselectPrestamosPageModule {}
