import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ClienteselectPrestamosPage } from './clienteselect-prestamos.page';

describe('ClienteselectPrestamosPage', () => {
  let component: ClienteselectPrestamosPage;
  let fixture: ComponentFixture<ClienteselectPrestamosPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClienteselectPrestamosPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ClienteselectPrestamosPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
