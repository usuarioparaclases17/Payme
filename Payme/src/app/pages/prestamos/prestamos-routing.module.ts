import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PrestamosPage } from './prestamos.page';
import {} from '../clienteselect-prestamos/clienteselect-prestamos.module';

const routes: Routes = [
  {
    path: 'tabs',
    component: PrestamosPage,
    children: [
      {
        path: 'clienteselect-prestamos',
        loadChildren: () => import('../clienteselect-prestamos/clienteselect-prestamos.module').then(m => m.ClienteselectPrestamosPageModule)
      }
    ]
  },
  {
    path: '',
    redirectTo: 'tabs/clienteselect-prestamos',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PrestamosPageRoutingModule {}
