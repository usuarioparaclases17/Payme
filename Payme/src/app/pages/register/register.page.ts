import { Component, OnInit } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import { User } from "../../models/Users";
import { Router } from "@angular/router";
import {
  FormControl,
  FormGroup,
  FormsModule,
  Validators
} from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";

@Component({
  selector: "app-register",
  templateUrl: "./register.page.html",
  styleUrls: ["./register.page.scss"]
})
export class RegisterPage implements OnInit {
  constructor(private authSvc: AuthService, private router: Router) {
    this.formularioDeRegistro = this.createFormGroup();
  }

  //Email pattern para comprobar
  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  // user: User = new User(); //Instanciando el objeto del usuario que se registrara.
  // public campos_incompletos: boolean = true;   //Instancia del formulario
  formularioDeRegistro: FormGroup;
  //selectedDate: string="";

  createFormGroup() {
    return new FormGroup({
      email: new FormControl("", [
        Validators.required,
        Validators.minLength(5),
        Validators.pattern(this.emailPattern)
      ]),
      nombre_completo: new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ]),
      nombre_usuario: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      contra: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ]),
      telefono: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(15)
      ]),
      celular: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.maxLength(15)
      ]),
      fecha_nacimiento: new FormControl("", Validators.required)
    });
  }

  ngOnInit() {}

  async registrarse() {
    const user = await this.authSvc.Registrarse_authSvc(
      this.formularioDeRegistro.value
    );
    if (user) {
      console.log("✅✅ Succesfully created user! ✅✅");
      this.router.navigateByUrl("/menu/inicio");
      this.LimpiarFormulario();
    }
  }

  LimpiarFormulario() {
    this.formularioDeRegistro.reset();
  }
}
