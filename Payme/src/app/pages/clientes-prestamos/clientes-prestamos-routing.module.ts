import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ClientesPrestamosPage } from './clientes-prestamos.page';

const routes: Routes = [
  {
    path: '',
    component: ClientesPrestamosPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ClientesPrestamosPageRoutingModule {}
