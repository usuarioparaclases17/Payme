import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClientesPrestamosPageRoutingModule } from './clientes-prestamos-routing.module';

import { ClientesPrestamosPage } from './clientes-prestamos.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ClientesPrestamosPageRoutingModule
  ],
  declarations: [ClientesPrestamosPage]
})
export class ClientesPrestamosPageModule {}
