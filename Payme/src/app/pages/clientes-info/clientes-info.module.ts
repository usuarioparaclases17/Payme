import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ClientesInfoPageRoutingModule } from './clientes-info-routing.module';

import { ClientesInfoPage } from './clientes-info.page';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [
    ReactiveFormsModule,
    CommonModule,
    FormsModule,
    IonicModule,
    ClientesInfoPageRoutingModule
  ],
  declarations: [ClientesInfoPage]
})
export class ClientesInfoPageModule {}
