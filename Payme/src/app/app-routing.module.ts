import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './guards/auth.guard';
import { ResolverService } from './services/resolver.service';
import { Resolve2Service } from './services/resolve2.service';
import { Resolve3Service } from  './services/resolve3.service';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';
import { ClientesPrestamosComponent } from './components/clientes/clientes-lista/clientes-prestamos/clientes-prestamos.component';
import { ClientesInfoComponent } from './components/clientes/clientes-lista/clientes-info/clientes-info.component';
import { CrearPrestamosComponent } from './components/prestamos/prestamos-lista-clientes/crear-prestamos/crear-prestamos.component';
import { PrestamosListaClientesComponent } from './components/prestamos/prestamos-lista-clientes/prestamos-lista-clientes.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)},
  { path: 'register', loadChildren: () => import('./pages/register/register.module').then( m => m.RegisterPageModule)},
  { path: 'menu', loadChildren: () => import('./pages/menu/menu.module').then( m => m.MenuPageModule), canActivate:[AuthGuard]},
  // { path: 'clientes-prestamos', component: ClientesPrestamosComponent, pathMatch: 'full'},           
  // { path: 'clientes-prestamos/:id', resolve: { special: ResolverService }, component: ClientesPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'clientes-info', component: ClientesInfoComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'clientes-info/:id', resolve: {special: Resolve3Service }, component: ClientesInfoComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'crear-prestamos-tab', component: CrearPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  // { path: 'crear-prestamos-tab/:id', resolve: { special: Resolve2Service }, component: CrearPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  // { path: 'clienteselect-prestamos', component: PrestamosListaClientesComponent, pathMatch: 'full'}

  { path: 'clientes-prestamos', loadChildren: () => import('./pages/clientes-prestamos/clientes-prestamos.module').then( m => m.ClientesPrestamosPageModule) },           
  { path: 'clientes-prestamos/:id', resolve: { special: ResolverService }, loadChildren: () => import('./pages/clientes-prestamos/clientes-prestamos.module').then( m => m.ClientesPrestamosPageModule), canActivate:[AuthGuard]},
  { path: 'clientes-info', loadChildren: () => import('./pages/clientes-info/clientes-info.module').then( m => m.ClientesInfoPageModule), canActivate:[AuthGuard]},
  { path: 'clientes-info/:id', resolve: {special: Resolve3Service }, loadChildren: () => import('./pages/clientes-info/clientes-info.module').then( m => m.ClientesInfoPageModule), canActivate:[AuthGuard]},
  { path: 'crear-prestamos-tab', loadChildren: () => import('./pages/crear-prestamos-tab/crear-prestamos-tab.module').then( m => m.CrearPrestamosTabPageModule), canActivate:[AuthGuard] },
  { path: 'crear-prestamos-tab/:id', resolve: { special: Resolve2Service }, loadChildren: () => import('./pages/crear-prestamos-tab/crear-prestamos-tab.module').then( m => m.CrearPrestamosTabPageModule), canActivate:[AuthGuard] },
  { path: 'clienteselect-prestamos', loadChildren: () => import('./pages/clienteselect-prestamos/clienteselect-prestamos.module').then( m => m.ClienteselectPrestamosPageModule)}

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
