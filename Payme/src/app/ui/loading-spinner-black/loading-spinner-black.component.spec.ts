import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoadingSpinnerBlackComponent } from './loading-spinner-black.component';

describe('LoadingSpinnerBlackComponent', () => {
  let component: LoadingSpinnerBlackComponent;
  let fixture: ComponentFixture<LoadingSpinnerBlackComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoadingSpinnerBlackComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoadingSpinnerBlackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
