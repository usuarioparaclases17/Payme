import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-loading-spinner-black',
  templateUrl: './loading-spinner-black.component.html',
  styleUrls: ['./loading-spinner-black.component.scss'],
})
export class LoadingSpinnerBlackComponent implements OnInit {

  constructor() { }

  ngOnInit() {}

}
