import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InformacionPagosComponent } from './informacion-pagos.component';

describe('InformacionPagosComponent', () => {
  let component: InformacionPagosComponent;
  let fixture: ComponentFixture<InformacionPagosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InformacionPagosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InformacionPagosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
