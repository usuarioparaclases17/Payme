import { Component, OnInit } from '@angular/core';
import { DbService } from "../../../../services/db.service";
import { Prestamo } from "../../../../models/Prestamo";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-crear-prestamos',
  templateUrl: './crear-prestamos.component.html',
  styleUrls: ['./crear-prestamos.component.css']
})
export class CrearPrestamosComponent implements OnInit {
  selectedVal = { Number: 1 };

  prestamo: Prestamo = new Prestamo();

  cliente_data: any;
  cliente_id: any;
  cliente_nombre: any;
  campos_incompletos: any;

  constructor(
    private router: Router,
    private DbSvc: DbService,
    private route: ActivatedRoute
  ) {}

  async ngOnInit() {
    await this.waitforData();
  }

  async waitforData() {
    if (this.route.snapshot.data["special"]) {
      this.cliente_data = this.route.snapshot.data["special"];
      this.cliente_nombre = this.cliente_data.nombre_completo;
      this.cliente_id = this.cliente_data.id_cliente;
    }
  }

  DIARIO = "diario";
  SEMANAL = "semanal";
  QUINCENAL = "quincenal";
  MENSUAL = "mensual";
  BIMESTRAL = "bimestral";
  TRIMESTRAL = "trimestral";
  CUATRIMESTRAL = "cuatrimestral";
  SEMESTRAL = "semestral";
  ANUAL = "anual";
  abono_al_capital: string;
  interes: string;
  valor_de_cuota: number;

  getTasa(tasa, tasa_tipo, periodo) {
    if (tasa_tipo == this.ANUAL) { tasa = tasa / 12 }
    tasa = tasa / 100.0
    if (periodo == this.DIARIO) { tasa = tasa / 30.4167 };
    if (periodo == this.SEMANAL) { tasa = tasa / 4.34524 };
    if (periodo == this.QUINCENAL) { tasa = tasa / 2 };
    if (periodo == this.MENSUAL) { };
    if (periodo == this.BIMESTRAL) { tasa = tasa * 2 };
    if (periodo == this.TRIMESTRAL) { tasa = tasa * 3 };
    if (periodo == this.CUATRIMESTRAL) { tasa = tasa * 4 };
    if (periodo == this.SEMESTRAL) { tasa = tasa * 6 };
    if (periodo == this.ANUAL) { tasa = tasa * 12 };
    return tasa;
  }

  getValorDeCuotaFija(monto, tasa, cuotas, periodo, tasa_tipo) {
    tasa = this.getTasa(tasa, tasa_tipo, periodo);
    const valor = monto *( (tasa * Math.pow(1 + tasa, cuotas)) / (Math.pow(1 + tasa, cuotas) - 1) );
    return valor.toFixed(2);
  }

getAmortizacion(monto, tasa, cuotas, periodo, tasa_tipo) {
 var valor_de_cuota = this.getValorDeCuotaFija(monto, tasa, cuotas, periodo, tasa_tipo);
 var saldo_al_capital = monto;
 var items = new Array();

 for (let i=0; i < cuotas; i++) {
     var interes = saldo_al_capital * this.getTasa(tasa, tasa_tipo, periodo);
     var abono_al_capital = this.valor_de_cuota - interes;
     saldo_al_capital -= abono_al_capital;
     var numero = i + 1;
     
     this.interes = interes.toFixed(2);
     this.abono_al_capital = abono_al_capital.toFixed(2);
     saldo_al_capital = saldo_al_capital.toFixed(2);

     var item = [numero, interes, abono_al_capital, valor_de_cuota, saldo_al_capital];
     items.push(item);
 }
 return items;
}


setMoneda(num) {
 num = num.toString().replace(/\$|\,/g, '');
 if (isNaN(num)) num = "0";
 var sign = (num == (num = Math.abs(num)));
 num = Math.floor(num * 100 + 0.50000000001);
 var cents = num % 100;
 num = Math.floor(num / 100).toString();
 if (cents < 10) cents = 0 + cents;
 for (var i = 0; i < Math.floor((num.length - (1 + i)) / 3); i++)
     num = num.substring(0, num.length - (4 * i + 3)) + ',' + num.substring(num.length - (4 * i + 3));
 return (((sign) ? '' : '-') + '$' + num + ((cents == 0) ? '' : '.' + cents));
}

  agregar_Prestamos() {



    function calcular() {
        var monto = this.prestamo.monto;
        var cuotas = this.prestamo.cuotas;
        var tasa = this.prestamo.porciento_interes;
        if (!monto) {
            alert('Indique el monto');
            return;
        }
        if (!cuotas) {
            alert('Indique las cuotas');
            return;
        }
        if (!tasa) {
            alert('Indique la tasa');
            return;
        }
        if (parseInt(cuotas) < 1) {
            alert('Las cuotas deben ser de 1 en adelante');
            return;
        }
        var select_periodo = this.prestamo.periodo_pago;
        var periodo = select_periodo;
        var select_tasa_tipo = this.prestamo.tipo_tasa;
        var tasa_tipo = select_tasa_tipo;
        var items = this.getAmortizacion(monto, tasa, cuotas, periodo, tasa_tipo);
        var tbody = document.getElementById("tbody_1");
        tbody.innerHTML = "";


if (parseInt(cuotas) > 3000) { alert("Ha indicado una cantidad excesiva de cuotas, porfavor reduzcala a menos de 3000"); return; }



        for (let i = 0; i < items.length; i++) {
            var item = items[i];
            var tr = document.createElement("tr");
            for (let e = 0; e < item.length; e++) {
                var value = item[e];
                if (e > 0) { value = this.setMoneda(value); }
                var td = document.createElement("td");
                var textCell = document.createTextNode(value);
                td.appendChild(textCell);
                tr.appendChild(td);
            }
            tbody.appendChild(tr);
        }
        var div1 = document.getElementById("div-valor-cuota");

        const valor = this.setMoneda(items[0][3]);
        div1.innerHTML = valor;
        var msg = "";
        if (periodo == "diario") { 
msg = "Usted estará pagando " + valor + ", todos los dias durante " + items.length + " dias.";
}
if (periodo == "semanal") {
msg = "Usted pagará " + valor + ", semanalmente durante " + items.length + " semanas.";
}
if (periodo == "mensual") {
msg = "Usted pagará " + valor + ", mensualmente durante " + items.length + " meses.";
}
if (periodo == "quincenal") {
msg = "Usted pagará " + valor + ", de manera quincenal por un periodo de " + items.length + " quincenas.";
}
if (periodo == "bimestral") {
msg = "Usted pagará " + valor + ", cada 2 meses durante un periodo de " + items.length + " bimestres.";
}
if (periodo == "trimestral") {
msg = "Usted va a pagar " + valor + ", cada 3 meses durante " + items.length + " trimestres.";
}
if (periodo == "cuatrimestral") {
msg = "Usted pagará " + valor + ", cada cuatrimestre (4 meses) por un periodo de " + items.length + " cuatrimestres.";
}
if (periodo == "semestral") {
msg = "Usted pagará " + valor + ", cada 6 meses durante " + items.length + " semestres";
}
if (periodo == "anual") {
msg = "Usted pagará " + valor + ", anualmente por un periodo de " + items.length + " años";
}
var div2 = document.getElementById("div-comentario");
div2.innerHTML = msg;
    }
    // const CapitalInicial = this.prestamo.monto;
    // const Tinteres = this.prestamo.porciento_interes;
    // const Years = this.prestamo.cuotas;

    // //calculando prestamo

    // const capital = parseFloat(CapitalInicial);
    // const calcularinteres = parseFloat(Tinteres) / 100 / 12;
    // const calcularpagos = parseFloat(Years) * 12;

    // const x = Math.pow(1 + calcularinteres, calcularpagos);
    // const mensual = (capital * x * calcularinteres) / (x - 1);
    // const pagoMensual1 = mensual.toFixed(2);

    // const InteresTotal1 = (mensual * calcularpagos - capital).toFixed(2);

    // //Compute Total Payment

    // const PagoTotal1 = (mensual * calcularpagos).toFixed(2);

    // this.prestamo.pagoMensual = pagoMensual1;
    // this.prestamo.interesTotal = InteresTotal1;
    // this.prestamo.pagoTotal = PagoTotal1;

    this.DbSvc.agregar_Prestamo(
      this.prestamo,
      this.cliente_id,
      this.cliente_nombre
    );
    this.router.navigateByUrl("/menu/inicio/tabs/clientes");

    this.prestamo.monto = null;
    this.prestamo.periodo_pago = "";
    this.prestamo.porciento_interes = null;
    this.prestamo.cuotas = null;
  }
}



