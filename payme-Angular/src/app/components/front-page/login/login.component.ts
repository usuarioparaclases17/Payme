import { Component, OnInit } from '@angular/core';
import { MatSidenavModule } from '@angular/material/sidenav';
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";
import { AuthService } from "../../../services/auth.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  ngOnInit() {}

  constructor(
    private authSvc: AuthService,
    private router: Router,
    public menu: MatSidenavModule
  ) {
    this.formularioDeInicioDeSesion = this.createFormGroup();
  }

  createFormGroup() {
    return new FormGroup({
      email: new FormControl("", [
        Validators.required,
        Validators.minLength(5),
        Validators.pattern(this.emailPattern)
      ]),
      contra: new FormControl("", [
        Validators.required,
        Validators.minLength(6)
      ])
    });
  }

  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  formularioDeInicioDeSesion: FormGroup;

  async onLoggin() {
    const user_isLogged = await this.authSvc.Logearse(
      this.formularioDeInicioDeSesion.value
    );

    if (user_isLogged) {
      console.log("✅✅ Succesfully logged user! ✅✅");
      this.router.navigateByUrl("/home");
    } else {
      console.log("❌❌ Unsuccesfully logged user...❌❌");
    }
  }
}
