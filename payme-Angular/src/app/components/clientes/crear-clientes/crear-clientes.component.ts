import { Component, OnInit } from '@angular/core';
import { DbService } from "../../../services/db.service";
import { Clients } from "../../../models/Clients";
import { Router } from "@angular/router";
import {
  FormControl,
  FormGroup,
  FormsModule,
  Validators
} from "@angular/forms";
import { ReactiveFormsModule } from "@angular/forms";

@Component({
  selector: 'app-crear-clientes',
  templateUrl: './crear-clientes.component.html',
  styleUrls: ['./crear-clientes.component.css']
})
export class CrearClientesComponent implements OnInit {
  public campos_incompletos: boolean = true;
  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  selectedVal: any[] = [];
  estadoLaboral: any[] = [];
  estadoCivil: any[] = [];

  cliente: Clients = new Clients();

  formularioDeCrearCliente: FormGroup;

  ngOnInit() {}
  constructor(
    private DbSvc: DbService,
  ) {
    this.formularioDeCrearCliente = this.createFormGroup();
  }

  createFormGroup() {
    return new FormGroup({
      //Informacion del Cliente
      nombre_completo: new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ]),
      celular: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      telefono: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      cedula: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      direccion: new FormControl("", [
        Validators.required,
        Validators.minLength(15)
      ]),
      estado_civil: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ]),
      correo_electronico: new FormControl("", [
        Validators.required,
        Validators.minLength(5),
        Validators.pattern(this.emailPattern)
      ]),
      estado_laboral: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ]),
      sueldo: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),
      modalidad_sueldo: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ]),
      ingresos_adicionales: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),

      //Informacion del PARIENTE del Cliente
      pariente_nombre: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      pariente_celular: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      pariente_email: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.pattern(this.emailPattern)
      ]),
      pariente_parentezco: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),

      //Informacion del GARANTE del Cliente
      garante_nombre: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      garante_celular: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      garante_email: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.pattern(this.emailPattern)
      ])
    });
  }

  async agregar_cliente() {
    await this.DbSvc.add_cliente(this.formularioDeCrearCliente.value);
    this.formularioDeCrearCliente.reset();
  }
}
