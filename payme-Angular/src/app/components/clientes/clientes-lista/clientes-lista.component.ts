import { Component, OnInit } from '@angular/core';
import { DbService } from "../../../services/db.service";
import { ActivatedRoute, Router } from "@angular/router";

@Component({
  selector: 'app-clientes-lista',
  templateUrl: './clientes-lista.component.html',
  styleUrls: ['./clientes-lista.component.css']
})
export class ClientesListaComponent implements OnInit {

  pages = [
    {
      title: "Inicio",
      url: "/clientes-tab/inicio"
    }
  ];

  CargandoClientesConPrestamo_spnner: boolean = true;
  CargandoClientesDisponibles_spnner: boolean = true;

  clientes_conPrestamo: any = [];
  clientes_disponibles: any = [];
  hay_clientes: boolean = true;

  constructor(
    private actroute: ActivatedRoute,
    private dbSvc: DbService,
    private router: Router
  ) {}

  ngOnInit() {
    this.getClients();
    this.ObtenerClientes_disponibles();
  }

  async getClients() {
    this.clientes_conPrestamo = await this.dbSvc.getClients_conPrestamos();

    this.CargandoClientesConPrestamo_spnner = false;
    this.verSiHayClientes_O_no();
  }

  verSiHayClientes_O_no() {
    if (this.clientes_conPrestamo && this.clientes_conPrestamo.length > 0) {
      this.hay_clientes = true;
    } else {
      this.hay_clientes = false;
    }
  }

  // REDIRIGIRSE AL PRESTAMO QUE TIENE ACTIVO ESE CLIENTE.
  async abrirPrestamosConServicio(
    id_cliente: string,
    nombre_cliente: string,
    cliente_info: any
  ) {
    await this.dbSvc.ObtenerDatosDelPrestamo(id_cliente, cliente_info);
    this.router.navigateByUrl(`/clientes-prestamos/${nombre_cliente}`);
  }

  // Ver los datos de ese cliente
  abrirInformacionCliente(cliente: any) {
    this.dbSvc.obtenerInformacionCliente(cliente);
    this.router.navigateByUrl(`/clientes-info/${cliente.nombre_completo}`);
  }

  async ObtenerClientes_disponibles() {
    this.clientes_disponibles = await this.dbSvc.getClients_sinPrestamos();
    this.CargandoClientesDisponibles_spnner = false;
  }
}
