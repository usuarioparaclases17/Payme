import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientesPrestamosComponent } from './clientes-prestamos.component';

describe('ClientesPrestamosComponent', () => {
  let component: ClientesPrestamosComponent;
  let fixture: ComponentFixture<ClientesPrestamosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientesPrestamosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientesPrestamosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
