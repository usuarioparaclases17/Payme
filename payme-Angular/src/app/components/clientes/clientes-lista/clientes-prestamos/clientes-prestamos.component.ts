import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Prestamo } from "../../../../models/Prestamo";
import { DbService } from "../../../../services/db.service";

@Component({
  selector: 'app-clientes-prestamos',
  templateUrl: './clientes-prestamos.component.html',
  styleUrls: ['./clientes-prestamos.component.css']
})
export class ClientesPrestamosComponent implements OnInit {
  all_data: any;
  prestamo_data: any;
  cliente_data: any;

  prestamo: Prestamo = new Prestamo();

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dbSvc: DbService
  ) {}

  ngOnInit() {
    this.waitforData();
    this.llenarvaloresdeinput(this.prestamo_data);
  }

  waitforData() {
    if (this.route.snapshot.data["special"]) {
      this.all_data = this.route.snapshot.data["special"];

      this.cliente_data = this.all_data[0];
      this.prestamo_data = this.all_data[1];
    } else {
    }
  }

  llenarvaloresdeinput(prestamo: any) {
    this.prestamo = Object.assign({}, prestamo);
  }

  async cancelar_Prestamo(cliente_id) {
    await this.dbSvc.cancelar_Prestamo(cliente_id);
    this.router.navigateByUrl("/clientes-lista");
  }

  async irACrearPrestamos() {
    this.router.navigateByUrl(
      `/crear-prestamos/${this.cliente_data.nombre_completo}`
    );
  }
}
