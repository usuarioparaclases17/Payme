import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { Clients } from "src/app/models/Clients";
import { DbService } from "src/app/services/db.service";
import { FormControl, FormGroup, Validators } from "@angular/forms";

@Component({
  selector: 'app-clientes-info',
  templateUrl: './clientes-info.component.html',
  styleUrls: ['./clientes-info.component.css']
})
export class ClientesInfoComponent implements OnInit {
  cliente_info: any;
  cliente_id: any;

  cliente: Clients = new Clients();

  constructor(
    private route: ActivatedRoute,
    private dbSvc: DbService,
    private router: Router
  ) {
    this.formularioDeActualizarInformacionDelCliente = this.createFormGroup();
  }

  ngOnInit() {
    this.waitforData();
    this.llenarvaloresdeinput(this.cliente_info);
  }

  waitforData() {
    if (this.route.snapshot.data["special"]) {
      this.cliente_info = this.route.snapshot.data["special"];
      this.cliente_id = this.cliente_info.id_cliente;
    } else {
    }
  }

  llenarvaloresdeinput(cliente: any) {
    this.cliente = Object.assign({}, cliente);
  }

  formularioDeActualizarInformacionDelCliente: FormGroup;
  emailPattern: any = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  createFormGroup() {
    return new FormGroup({
      //Informacion del Cliente
      nombre_completo: new FormControl("", [
        Validators.required,
        Validators.minLength(5)
      ]),
      celular: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      telefono: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      cedula: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      direccion: new FormControl("", [
        Validators.required,
        Validators.minLength(15)
      ]),
      estado_civil: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ]),
      correo_electronico: new FormControl("", [
        Validators.required,
        Validators.minLength(5),
        Validators.pattern(this.emailPattern)
      ]),
      estado_laboral: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ]),
      sueldo: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),
      modalidad_sueldo: new FormControl("", [
        Validators.required,
        Validators.minLength(4)
      ]),
      ingresos_adicionales: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),

      //Informacion del PARIENTE del Cliente
      pariente_nombre: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      pariente_celular: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      pariente_email: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.pattern(this.emailPattern)
      ]),
      pariente_parentezco: new FormControl("", [
        Validators.required,
        Validators.minLength(2)
      ]),

      //Informacion del GARANTE del Cliente
      garante_nombre: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      garante_celular: new FormControl("", [
        Validators.required,
        Validators.minLength(10)
      ]),
      garante_email: new FormControl("", [
        Validators.required,
        Validators.minLength(10),
        Validators.pattern(this.emailPattern)
      ])
    });
  }

  ActualizarCliente() {
    this.dbSvc.updateCliente(this.cliente, this.cliente_id);
    this.router.navigateByUrl("/clientes-lista");
  }
}
