import { Component, OnInit } from '@angular/core';
import { Router, RouterEvent } from "@angular/router";
import { AuthService } from "../../services/auth.service";
import { BreakpointObserver, Breakpoints, BreakpointState } from "@angular/cdk/layout";
import { Observable } from 'rxjs';
import { shareReplay, map } from 'rxjs/operators';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  pages = [
    {
      title: "Inicio",
      url: "menu/inicio"
    },
    {
      title: "Crear Prestamos",
      url: "menu/prestamos"
    },
    {
      title: "Clientes",
      url: "menu/clientes"
    }
  ];

  selectedPath = "";

  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
  .pipe(
    map(result => result.matches),
    shareReplay()
  );

  constructor(private breakpointObserver: BreakpointObserver,private router: Router, private authSvc: AuthService) {
    this.router.events.subscribe((event: RouterEvent) => {
      if (event && event.url) {
        this.selectedPath = event.url;
      }
    });
  }

  ngOnInit() {}

  desLogearse() {
    this.authSvc.desLogearse();
  }
}
