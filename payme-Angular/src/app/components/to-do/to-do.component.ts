import { Component, OnInit } from '@angular/core';
import { Item } from '../../models/Item';


@Component({
  selector: 'app-to-do',
  templateUrl: './to-do.component.html',
  styleUrls: ['./to-do.component.css']
})
export class ToDoComponent implements OnInit {


  ngOnInit() {

  }

  public todos: Item[] = [];
  public description: string;


  addNewTodo(): void {
    this.todos.push({
      description: this.description,
      isDone: false
    });
    this.description = null;
  }

}