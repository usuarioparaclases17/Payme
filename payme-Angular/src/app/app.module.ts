import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

//Componentes
import { ClientesComponent } from './components/clientes/clientes.component';
import { ClientesListaComponent } from './components/clientes/clientes-lista/clientes-lista.component';
import { CrearClientesComponent } from './components/clientes/crear-clientes/crear-clientes.component';
import { ClientesInfoComponent } from './components/clientes/clientes-lista/clientes-info/clientes-info.component';
import { ClientesPrestamosComponent } from './components/clientes/clientes-lista/clientes-prestamos/clientes-prestamos.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { HomeComponent } from './components/inicio/home/home.component';
import { TableroComponent } from './components/inicio/tablero/tablero.component';
import { PagosRealizadosComponent } from './components/pagos-realizados/pagos-realizados.component';
import { PagosComponent } from './components/pagos-realizados/pagos/pagos.component';
import { InformacionPagosComponent } from './components/pagos-realizados/pagos/informacion-pagos/informacion-pagos.component';
import { PrestamosComponent } from './components/prestamos/prestamos.component';
import { PrestamosListaClientesComponent } from './components/prestamos/prestamos-lista-clientes/prestamos-lista-clientes.component';
import { CrearPrestamosComponent } from './components/prestamos/prestamos-lista-clientes/crear-prestamos/crear-prestamos.component';
import { LoginComponent } from './components/front-page/login/login.component';
import { RegisterComponent } from './components/front-page/register/register.component';
import { MenuComponent } from './components/menu/menu.component';
import { LoadingSpinnerComponent } from './ui/loading-spinner/loading-spinner.component';

//BD y rutas
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar'; 
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

//Material Design Imports
import { MatListModule } from '@angular/material/list';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatInputModule } from '@angular/material/input';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatDividerModule } from '@angular/material/divider';
import { LayoutModule } from '@angular/cdk/layout';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import {MatSelectModule} from '@angular/material/select';
import {MatGridListModule} from '@angular/material/grid-list';
import { ToDoComponent } from './components/to-do/to-do.component';

  //Database config
  const firebaseConfig = {
    apiKey: "AIzaSyBnckz_VJ9HJC5eVU3b7O6ttUlfaw6X_F0",
    authDomain: "payme-67dd4.firebaseapp.com",
    databaseURL: "https://payme-67dd4.firebaseio.com",
    projectId: "payme-67dd4",
    storageBucket: "payme-67dd4.appspot.com",
    messagingSenderId: "924364958976",
    appId: "1:924364958976:web:ffa61f466c069df44257e5"
  };

@NgModule({
  declarations: [
    AppComponent,
    ClientesComponent,
    ClientesListaComponent,
    CrearClientesComponent,
    ClientesInfoComponent,
    ClientesPrestamosComponent,
    InicioComponent,
    HomeComponent,
    TableroComponent,
    PagosRealizadosComponent,
    PagosComponent,
    InformacionPagosComponent,
    PrestamosComponent,
    PrestamosListaClientesComponent,
    CrearPrestamosComponent,
    LoginComponent,
    RegisterComponent,
    MenuComponent,
    LoadingSpinnerComponent,
    ToDoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FormsModule,
    ReactiveFormsModule,
    MatListModule,
    MatFormFieldModule,
    MatSnackBarModule,
    MatSidenavModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatDividerModule,
    MatMenuModule,
    MatSelectModule,
    MatGridListModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule, // firestore
    AngularFireAuthModule, // auth
    AngularFireStorageModule,
    NgbModule,
    LayoutModule,
    MatIconModule // storage
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
