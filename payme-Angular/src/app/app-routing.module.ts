import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';
import { LoginComponent } from './components/front-page/login/login.component';
import { MenuComponent } from './components/menu/menu.component';
import { RegisterComponent } from './components/front-page/register/register.component';
import { AuthGuard } from './guards/auth.guard';
import { ClientesPrestamosComponent } from './components/clientes/clientes-lista/clientes-prestamos/clientes-prestamos.component';
import { ResolverService } from './services/resolver.service';
import { ClientesInfoComponent } from './components/clientes/clientes-lista/clientes-info/clientes-info.component';
import { Resolve3Service } from './services/resolve3.service';
import { CrearPrestamosComponent } from './components/prestamos/prestamos-lista-clientes/crear-prestamos/crear-prestamos.component';
import { Resolve2Service } from './services/resolve2.service';
import { PrestamosListaClientesComponent } from './components/prestamos/prestamos-lista-clientes/prestamos-lista-clientes.component';
import { HomeComponent } from './components/inicio/home/home.component';
import { ClientesComponent } from './components/clientes/clientes.component';
import { ClientesListaComponent } from './components/clientes/clientes-lista/clientes-lista.component';
import { CrearClientesComponent } from './components/clientes/crear-clientes/crear-clientes.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { TableroComponent } from './components/inicio/tablero/tablero.component';
import { PagosRealizadosComponent } from './components/pagos-realizados/pagos-realizados.component';
import { PagosComponent } from './components/pagos-realizados/pagos/pagos.component';
import { InformacionPagosComponent } from './components/pagos-realizados/pagos/informacion-pagos/informacion-pagos.component';
import { PrestamosComponent } from './components/prestamos/prestamos.component';
import { ToDoComponent } from './components/to-do/to-do.component';


const routes: Routes = [
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, pathMatch: 'full'},
  { path: 'register',component: RegisterComponent, pathMatch: 'full'},
  { path: 'menu', component: MenuComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  { path: 'to-do', component: ToDoComponent, pathMatch: 'full', canActivate:[AuthGuard]},

  //Clientes
  //{ path: 'clientes', component: ClientesComponent, pathMatch: 'full' },    -NO SE UTILIZA-
  { path: 'clientes-lista', component: ClientesListaComponent , pathMatch: 'full', canActivate:[AuthGuard] },
  { path: 'crear-clientes', component: CrearClientesComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  { path: 'clientes-prestamos', component: ClientesPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard] },           
  { path: 'clientes-prestamos/:id', resolve: { special: ResolverService }, component: ClientesPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  { path: 'clientes-info', component: ClientesInfoComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  { path: 'clientes-info/:id', resolve: {special: Resolve3Service }, component: ClientesInfoComponent, pathMatch: 'full', canActivate:[AuthGuard] },

  //Prestamos
  //{ path: 'prestamos', component: PrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard] },    -NO SE UTILIZA-
  { path: 'prestamos-lista-clientes', component: PrestamosListaClientesComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  { path: 'crear-prestamos', component: CrearPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  { path: 'crear-prestamos/:id', resolve: { special: Resolve2Service }, component: CrearPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard] },

  //Inicio
  //{ path: 'inicio', component: InicioComponent, pathMatch: 'full', canActivate:[AuthGuard] },    -NO SE UTILIZA-
  { path: 'home', component: HomeComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  { path: 'dashboard', component: TableroComponent, pathMatch: 'full', canActivate:[AuthGuard] },

  //Pagos Realizados
  //{ path: 'pagos-realizados', component: PagosRealizadosComponent, pathMatch: 'full', canActivate:[AuthGuard] },    -NO SE UTILIZA-
  { path: 'pagos', component: PagosComponent, pathMatch: 'full', canActivate:[AuthGuard] },
  { path: 'informacion-pagos', component: InformacionPagosComponent, pathMatch: 'full', canActivate:[AuthGuard] },

  



  // { path: 'menu', component: MenuComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'clientes-prestamos', component: ClientesPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard]},           
  // { path: 'clientes-prestamos/:id', resolve: { special: ResolverService }, component: ClientesPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'clientes-info', component: ClientesInfoComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'clientes-info/:id', resolve: {special: Resolve3Service }, component: ClientesInfoComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'crear-prestamos', component: CrearPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'crear-prestamos/:id', resolve: { special: Resolve2Service }, component: CrearPrestamosComponent, pathMatch: 'full', canActivate:[AuthGuard]},
  // { path: 'prestamos-lista-clientes', component: PrestamosListaClientesComponent, pathMatch: 'full', canActivate:[AuthGuard]},
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
