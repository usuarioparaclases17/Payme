export class User {

    contra: string;
    email: string;
    nombre_completo: string;
    telefono: string;
    celular: string;
    nombre_usuario: string;
    fecha_nacimiento: Date;
    
}