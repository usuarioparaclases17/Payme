import { TestBed } from '@angular/core/testing';

import { Resolve2Service } from './resolve2.service';

describe('Resolve2Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Resolve2Service = TestBed.get(Resolve2Service);
    expect(service).toBeTruthy();
  });
});
