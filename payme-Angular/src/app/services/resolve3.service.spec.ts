import { TestBed } from '@angular/core/testing';

import { Resolve3Service } from './resolve3.service';

describe('Resolve3Service', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: Resolve3Service = TestBed.get(Resolve3Service);
    expect(service).toBeTruthy();
  });
});
